#! /bin/sh

# (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
#
# All Rights Reserved. See copyright.txt for details and a complete list of authors.
# Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

# This file sets permissions and creates relevant folders for Tiki.

# ---------------------------------------------------------

ADMIN_PATH=admin
DB_PATH=db
DOC_PATH=doc
FILES_PATH=files
IMG_PATH=img
IMG_WIKI_PATH=img/wiki
IMG_WIKI_UP_PATH=img/wiki_up
IMG_TRACKERS_PATH=img/trackers
INSTALLER_PATH=installer
LANG_PATH=lang
LIB_PATH=lib
MODULES_PATH=modules
MODS_PATH=mods
PERMISSIONCHECK_PATH=permissioncheck
STORAGE_PATH=storage
STORAGE_PUBLIC_PATH=storage/public
STORAGE_FGAL_PATH=storage/fgal
STORAGE_PUBLIC_H5P_PATH=storage/public/h5p
TEMP_PATH=temp
TEMP_CACHE_PATH=temp/cache
TEMP_PUBLIC_PATH=temp/public
TEMP_TEMPLATES_C_PATH=temp/templates_c
TEMP_UNIFIED_INDEX_PATH=temp/unified-index
TEMPLATES_PATH=templates
TESTS_PATH=tests
THEMES_PATH=themes
TIKI_TESTS_TESTS_PATH=tiki_tests/tests
VENDOR_PATH=vendor
VENDOR_CUSTOM_PATH=vendor_custom
WHELP_PATH=whelp

# EOF
