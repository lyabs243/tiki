// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of
// authors. Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See
// license.txt for details. $Id: $

$(document).on("ready tiki.modal.redraw", function () {

    var $start = $("#start"),
        $startPicker = $start.nextAll("input[type=text]"),
        $end = $("#end"),
        $endPicker = $end.nextAll("input[type=text]"),
        $frm_edit_calendar = $('form[id="editcalitem"]'),
        $copy_to_new_event_button = $('a[id="copy_to_new_event"]');

    //add browser timezone to hidden input
    $("input[name=tzoffset]").val((new Date()).getTimezoneOffset());

    $("#allday").change(function () {
        if ($(this).prop("checked")) {
            $(".time").css("visibility", "hidden");
            $startPicker.datepicker("disableTimepicker").datepicker("refresh").change();
            $endPicker.datepicker("disableTimepicker").datepicker("refresh").change();
        } else {
            $(".time").css("visibility", "visible");
            $startPicker.datepicker("enableTimepicker").datepicker("refresh").change();
            $endPicker.datepicker("enableTimepicker").datepicker("refresh").change();
        }
    });

    setTimeout(function () {
        $("#allday").change();
    }, 100);

    $("#durationBtn").off("click").click(function () {
        if ($(".duration.time:visible").length) {
            $(".duration.time").hide();
            $(".end").show();
            $(this).text(tr("Show duration"));
            $("#end_or_duration").val("end");
        } else {
            $(".duration.time").show();
            $(".end").hide();
            $(this).text(tr("Show end time"));
            $("#end_or_duration").val("duration");
        }
        return false;
    });

    var getEventTimes = function () {
        var out = {},
            start = parseInt($start.val()),
            end = parseInt($end.val());
        if (start) {
            out.start = new Date(start * 1000);
        } else {
            out.start = null;
        }
        if (end) {
            out.end = new Date(end * 1000);
        } else {
            out.end = null;
        }
        if (start && end) {
            out.duration = ($("select[name=duration_Hour]").val() * 3600) +
                ($("select[name=duration_Minute]").val() * 60);    // in seconds
        }

        return out;
    };

    var fNum = function (num) {
        var str = "0" + num;
        return str.substring(str.length - 2);
    };

    $(".duration.time select, #start").change(
        function () {
            const $this = $(this);
            // filter change event noise (from jquery-ui?)
            if ($this.data("prev-value") === undefined) {
                $this.data("prev-value", $this.val());
            } else if ($this.data("prev-value") === $this.val()) {
                return;
            }
            let times = getEventTimes();

            if (times.duration) {
                times.end = new Date(
                    times.start.getTime() + (times.duration * 1000)
                );

                $end.data("ignoreevent", true);

                $endPicker
                    .datepicker(
                        "setDate", times.end)
                    .datepicker("refresh").change();
                $end.val(times.end.getTime() / 1000);
            }

            $start.val(times.start.getTime() / 1000);
        });

    $end.change(function (event) {
        const $this = $(this);
        // filter change event noise (from jquery-ui?)
        if ($this.data("prev-value") === undefined) {
            $this.data("prev-value", $this.val());
        } else if ($this.data("prev-value") === $this.val()) {
            return;
        }

        let times = getEventTimes(),
            s = times.start ? times.start.getTime() : null,
            e = times.end ? times.end.getTime() : null;

        if ($end.data("ignoreevent")) {
            $end.removeData("ignoreevent");
            return;
        }
        if (e && e <= s) {
            $startPicker
                .datepicker("setDate", times.end);
                //.datepicker("refresh");
            $start.val(times.end.getTime() / 1000);
            s = e;
        }
        if (e) {
            times.duration = (e - s) / 1000;
            $("select[name=duration_Hour]").val(fNum(Math.floor(times.duration / 3600)))
                .trigger("change.select2");
            $("select[name=duration_Minute]").val(fNum(Math.floor((times.duration % 3600) / 60)))
                .trigger("change.select2");
        } else {
            $("select[name=duration_Hour]").val(1).trigger("change.select2");
            $("select[name=duration_Minute]").val(0).trigger("change.select2");
        }
    }).change();    // set duration on load

    // recurring events
    var $recurrentCheckbox = $("#id_recurrent");

    $recurrentCheckbox.click(function () {
        if ($(this).prop("checked")) {
            $("#recurrenceRules").show();

            // set inputs for a new recurring rule
            var d = new Date(parseInt($start.val() * 1000));
            $("select[name=weekday]").val(d.getDay()).trigger(
                "change.select2");
            $("select[name=dayOfMonth]").val(d.getDate()).trigger(
                "change.select2");
            $("select[name=dateOfYear_day]").val(d.getDate()).trigger(
                "change.select2");
            $("select[name=dateOfYear_month]").val(d.getMonth() + 1)
                .trigger("change.select2");

        } else {
            $("#recurrenceRules").hide();
        }
    });

    if (typeof $.validator !== "undefined") {
        $.validator.classRuleSettings.date = false;
    }

    if (typeof CKEDITOR === "object") {
        CKEDITOR.on("instanceReady", function (event) {
            // not sure why but the text area doesn't get its display:none applied when using full calendar
            event.editor.element.$.hidden = true;
        });
    }

    const addParticipant = function (participant) {
        if ($('#participant_roles tr.'+participant).length === 0) { // using class as data doesn't seem to work here
            let $newRow = $("#participant-template-row").clone(true, true);

            $("#participant_roles").append($newRow).trigger("change.select2");

            $newRow
                .removeClass("d-none noselect2")
                .removeAttr("id")
                .data("user", participant)
                .addClass(participant)
                .find("select[name='calitem[participant_roles]'").attr("name", `calitem[participant_roles][${participant}]`);

            $newRow
                .find("select[name='calitem[participant_partstat]'").attr("name", `calitem[participant_partstat][${participant}]`);

            $newRow
                .find("td.username").text(participant);

            $newRow.applySelect2();

        }
    };

    $('#participant_roles').on('click', '.delete-participant', function(e) {
        e.preventDefault();
        var $tr = $(this).closest('tr');
        $('select[name="save[participants][]"]').find('option[value="'+$tr.data('user')+'"]').prop("selected", false);
        $tr.remove();
        return false;
    });

    $('select[name="participants[]"]').change(function() {
        var users = $(this).val();
        for (var i = 0, l = users.length; i < l; i++) {
            addParticipant(users[i]);
        }
        var $sel = $(this);
        $('#participant_roles tr').each(function(idx, tr) {
            var user = $(tr).data('user');
            if (! user) {
                return;
            }
            if ($sel.find("option[value='"+user+"']").length > 0 && users.indexOf(user) === -1) {
                $(tr).remove();
            }
        });
    });

    $('input[name="participants"]').on("autocompleteselect", function( event, ui ) {
        addParticipant(ui.item.value);
    });

    $('#invite_emails').on('click', function() {
        var email = $('#add_participant_email').val();
        if (email) {
            addParticipant(email);
        }
        $('#add_participant_email').val('');
    });

    // process form submits etc
    $(document).on("click", ".edit-event-form input[type=submit]", function (event) {
        // add a hidden input version of this form as jQuery.fn.serialize doesn't include the clicked button
        const $this = $(this);
        if ($this.data("lastclicked") === event.timeStamp) {
            return false;
        }
        $this.data("lastclicked", event.timeStamp);

        const $form = $this.parents("form");
        const $modal = $form.parents(".modal");
        const btnName = $this.attr("name");
        $form.find("#act").val(btnName);

        // tell ckeditor to update the hidden textarea
        const $textarea = $form.find("textarea.wikiedit");
        if (typeof CKEDITOR !== "undefined" && CKEDITOR.instances[$textarea.attr("id")]) {
            const editor = CKEDITOR.instances[$textarea.attr("id")];
            if (editor.updateElement) { // sometimes fn not there?
                editor.updateElement();
            }
        }

        if (btnName === "preview") {
            // also used when changing calendars to reload the form
            $.post(
                $form.attr("action"),
                $form.serialize(),
                function (html) {
                    $("body").append(
                        $("<div class='d-none' id='editCalTemp'>").html(html)
                    );
                    const $editCalTemp = $("#editCalTemp");
                    $form.tikiModal().find(".form-contents").html($editCalTemp.find(".form-contents").html());
                    $editCalTemp.remove();
                    $modal.trigger("tiki.modal.redraw");
                    $modal.animate({
                        scrollTop: $modal.find(".preview").offset().top
                    }, 1000);
                },
                "html"
            );
            event.preventDefault();
            return false;
        } else {
            $form.submit();
            return true;
        }
    });

    window.calendarEditSubmit = function (data, form) {
        $.closeModal();
        if (data.url) {
            location.href = data.url;
        } else {
            setTimeout(function () {
                calendar.refetchEvents();
            }, 500);
        }
    };

    // avoid the "leave page" warning when copying to a new event
    $copy_to_new_event_button.click(function(){
        window.needToConfirm = false;
    });

    // reset confirm
    window.needToConfirm = false;
    $("input, select, textarea", "#editcalitem").change(function () {
        window.needToConfirm = true;
    });
});


/**
 * Checks recurring dates are valid
 *
 * @param day
 * @param month
 */
function checkDateOfYear(day, month)
{
    var mName = [
        "-",
        tr("January"),
        tr("February"),
        tr("March"),
        tr("April"),
        tr("May"),
        tr("June"),
        tr("July"),
        tr("August"),
        tr("September"),
        tr("October"),
        tr("November"),
        tr("December")
    ];
    var error = false;

    month = parseInt(month);
    day = parseInt(day);

    if (month === 4 || month === 6 || month === 9 || month === 11) {
        if (day === 31) {
            error = true;
        }
    }
    if (month === 2) {
        if (day > 29) {
            error = true;
        }
    }
    if (error) {
        $("#errorDateOfYear").text(
            tr("There's no such date as") + " " + day + " " + tr('of') + " "
            + mName[month]).show();
    } else {
        $("#errorDateOfYear").text("").hide();
    }
}

