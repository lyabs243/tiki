stages:
  - build
  - lint
  - tiki-check
  - unit-tests
  - package-tiki
  - vendors-security
  - vendors_update

variables:
  MYSQL_ROOT_PASSWORD: secret
  MYSQL_DATABASE: tikitest
  MYSQL_USER: tikiuser
  MYSQL_PASSWORD: tikipass
  ELASTICSEARCH_HOST: elasticsearch
  #The source for these images is at https://github.com/rjsmelo/docker-ubuntu-php
  #Would be nice to update them from code, similar to https://medium.com/devops-with-valentine/how-to-build-a-docker-image-and-push-it-to-the-gitlab-container-registry-from-a-gitlab-ci-pipeline-acac0d1f26df
  BASE_QA_IMAGE: xorti/ubuntu-php:8.1-qa
  #DBDiff is sensitive to mysql version.  
  #The schemas dumped for prior tikis on https://gitlab.com/tikiwiki/tikiwiki-ci-databases have explicit COLLATION() statements for each table column.  Later versions of mysql (8.0.11+, but not sure) will not dump the same whits explicit collations.
  #Running php doc/devtools/check_schema_upgrade.php -m 22 --db1=tiki:tiki@localhost:tikiold --db2=tiki:tiki@localhost:tikinew gives other errors localy that are false positives.  But locally, unless you decompress https://gitlab.com/tikiwiki/tikiwiki-ci-databases into doc/devtool/dbdiff/cache, tiki.org won't give the schemas to you, and will blacklist your IP address!  benoitg - 2023-05-16

  MYSQL_8_IMAGE: mysql:8.0.11
  MYSQL_5_IMAGE: mysql:5.6
  MANTICORE_DSN: http://manticore
  # You can set USE_GITLAB_PROXY to "true" in your project configuration to use the gitlab dependency proxy. (default not using as it may cause issues for some accounts)
  USE_GITLAB_PROXY: ''
  # Default value for DEPENDENCY_PROXY_PREFIX (not using any proxy), you can set this on your project also if you do not set USE_GITLAB_PROXY to "true"
  DEPENDENCY_PROXY_PREFIX: ''

workflow:
  rules:
    # IGNORE PUSHES ON NON VERSION BRANCHES
    - if: '$CI_PIPELINE_SOURCE == "push" && ($CI_COMMIT_BRANCH != "master" && $CI_COMMIT_BRANCH !~ /^\d*\.x$/)'
      when: never
    #CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX was terribly defined by gitlab, because you need to append a / to every image name.
    #So you can't just use CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX prepended to every image name.
    #The following is a workround: 
    #gitlab-ci-local doesn't support rules variables yet, so it skips anyway
    #https://github.com/firecow/gitlab-ci-local/issues/691
    #
    #But since DEPENDENCY_PROXY_PREFIX is set to the empty string above, 
    #DEPENDENCY_PROXY_PREFIX will have no effect
    #This will run in gitlab-ci-local
    #
    - if: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX != "" && $USE_GITLAB_PROXY == 'true'
      variables:
        DEPENDENCY_PROXY_PREFIX: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/
    # THIS WILL TRIGGER PIPELINES FOR MERGE REQUESTS AND SCHEDULES, AND PUSHES ON VERSION BRANCHES LIKE 21.x. 18.x
    - when: always
#
# build
#

composer:
  stage: build
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  variables:
    COMPOSER_DISCARD_CHANGES: "true"
    COMPOSER_NO_INTERACTION: "1"
    #Be carefull here, composer cache dir handling can be a bit surprising.
    #If cache dir is a relative path, it's relative to the working directory, or the -d parameter if specified.
    #If cache dir in an absolute path, it's used as is.
    #Since we use multiple vendor directories, and gitlab-ci has different rules for cache, and we want a global cache, we use ${CI_PROJECT_DIR} here so it's always at the root
    COMPOSER_CACHE_DIR: ${CI_PROJECT_DIR}/.composercache
  script:
    - composer -V | grep "version 2" || composer self-update --2
    - composer --ansi install -d vendor_bundled --no-progress --prefer-dist --no-interaction
  cache:
    - key:
        prefix: "composer"
        files:
          - vendor_bundled/composer.lock
      paths:
        - .composercache/
      policy: pull-push
    - key:
        prefix: "vendor_bundled"
        files:
          - vendor_bundled/composer.lock
      paths:
        - vendor_bundled/vendor/
      policy: pull-push
  artifacts:
    paths:
      - vendor_bundled/vendor/
    expire_in: 7 days
    when: always

composer-dbdiff:
  stage: build
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  variables:
    COMPOSER_DISCARD_CHANGES: "true"
    COMPOSER_NO_INTERACTION: "1"
    COMPOSER_CACHE_DIR: ${CI_PROJECT_DIR}/.composercache-dbdiff
  script:
    - composer -V | grep "version 2" || composer self-update --2
    - composer --ansi install -d doc/devtools/dbdiff/ --no-progress --prefer-dist -n
  cache:
    key:
      prefix: "composer-dbdiff"
      files:
        - doc/devtools/dbdiff/composer.lock
    paths:
      - .composercache-dbdiff/
    policy: pull-push
  artifacts:
    paths:
      - doc/devtools/dbdiff/vendor/
    expire_in: 7 days
    when: always

#
# Lint
#

phpcs:
  stage: lint
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - env
    - php vendor_bundled/vendor/squizlabs/php_codesniffer/bin/phpcs --cache=phpcs.cache -s --runtime-set ignore_warnings_on_exit true --parallel=8
  dependencies:
    - composer
  needs:
    - composer
  cache:
    - key:
        prefix: "phpcs"
        files:
          - phpcs.xml.dist
      paths:
        - phpcs.cache
      policy: pull-push

phplint81:
  stage: lint
  image: ${DEPENDENCY_PROXY_PREFIX}xorti/ubuntu-php:8.1-qa
  script:
    - php vendor_bundled/vendor/overtrue/phplint/bin/phplint -n --configuration=doc/devtools/phplint.yml
  dependencies:
    - composer
  needs:
    - composer
  cache:
    key: phplint81
    paths:
      - phplint.cache

smarty-lint:
  stage: lint
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - php vendor_bundled/vendor/umakantp/smartylint/smartyl -p --files=lib/test,templates,themes --rules=doc/devtools/smartyl.rules.xml
  dependencies:
    - composer
  needs:
    - composer

eslint:
  stage: lint
  image:
    name: ${DEPENDENCY_PROXY_PREFIX}cytopia/eslint:latest
    entrypoint: ["/bin/sh", "-c"]
  script:
    - eslint .
  dependencies: []
  needs: []
  tags:
    - docker

#
# Check Tikiwiki development specific check (related also with release)
#

sql-engine:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - php -d display_errors=On doc/devtools/check_sql_engine.php
  dependencies: []
  needs: []

schema-sql-drop:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - php -d display_errors=On doc/devtools/check_schema_sql_drop.php
  dependencies: []
  needs: []

schema-naming-convention:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - php -d display_errors=On doc/devtools/check_schema_naming_convention.php
  dependencies: []
  needs: []

translation:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - git log --first-parent --pretty="format:%h" -1 --skip=1 | xargs -I gitHash php -d display_errors=On console.php translation:englishupdate --diff-command="git diff gitHash" --git --audit
  dependencies:
    - composer
  needs:
    - composer
  allow_failure: true

translation-standards:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - php doc/devtools/check_template_translation_standards.php --all
  dependencies:
    - composer
  needs:
    - composer

check-bom-encoding:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - php doc/devtools/check_bom_encoding.php
  dependencies: []
  needs: []

unix-ending-line:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  script:
    - php doc/devtools/check_unix_ending_line.php
  dependencies: []
  needs: []

check-directories:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: tiki-check
  script:
    - php doc/devtools/check_tiki_directories.php
  dependencies: []
  needs: []

#This is a non-network check
composer-validates:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: tiki-check
  script:
    - composer -V | grep "version 2" || composer self-update --2
    - composer validate -d vendor_bundled --no-check-all
  dependencies: []
  needs: []

#This tests if composer COULD update the current composer.lock (it is coherent and installable).
#This is a network check, it could fail because composer.tiki.org is not up to date or some other non-code related reason.
composer-could-update-lock:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  variables:
    COMPOSER_CACHE_DIR: ${CI_PROJECT_DIR}/.composercache
  stage: tiki-check
  script:
    - composer -V | grep "version 2" || composer self-update --2
    - composer update -d vendor_bundled --lock --dry-run
  dependencies:
    - composer
  needs:
    - composer
  cache:
    - key:
        prefix: "composer"
        files:
          - vendor_bundled/composer.lock
      paths:
        - .composercache/
      #We need push, because otherwise repo information isn't cached
      policy: pull-push
  #This test should only run when composer-validates fail (because changes in the upstream deps can make this fail without code changes).  But I could find no way to have a test only run in gitlab-ci if a specific one fails.  So allow it to fail like composer-lock for now - benoitg 2023-05-03
  allow_failure: true

#This tests if composer WOULD update the current composer.lock.  This should only be important at release
.composer-lock:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: tiki-check
  variables:
      COMPOSER_CACHE_DIR: ${CI_PROJECT_DIR}/.composercache
  script:
    - cp $(command -v composer) temp/composer.phar
    - php temp/composer.phar -V | grep "version 2" || php temp/composer.phar self-update --2
    - php doc/devtools/update_composer_lock.php
  before_script:
    - mkdir before || true
    - cp vendor_bundled/composer.json vendor_bundled/composer.lock before
  after_script:
    - mkdir after || true
    - cp vendor_bundled/composer.json vendor_bundled/composer.lock after
    - diff -u before/composer.lock after/composer.lock
  dependencies:
    - composer
  needs:
    - composer
  artifacts:
    paths:
      - before
      - after
    expire_in: 7 days
    when: always
  cache:
    - key:
        prefix: "composer"
        files:
          - vendor_bundled/composer.lock
      paths:
        - .composercache/
      policy: pull-push
  allow_failure:
      exit_codes: 1

.template-tiki-schema-upgrade: &template-tiki-schema-upgrade
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: tiki-check
  services:
    - name: ${DEPENDENCY_PROXY_PREFIX}$IMAGE
      alias: mysql,
      command: ["--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci"]
  script:
    - 'mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql --skip-column-names -B -e "SELECT CONCAT(''Version: '', VERSION(), CHAR(13), ''sql_mode: '', @@GLOBAL.sql_mode)"'
    - echo "GRANT ALL ON tikiold.* TO '${MYSQL_USER}';" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - echo "GRANT ALL ON tikinew.* TO '${MYSQL_USER}';" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - '[ ! -d doc/devtools/dbdiff/cache ] && mkdir doc/devtools/dbdiff/cache'
    - '[ ! -f doc/devtools/dbdiff/cache/$DBFILE ] && curl -sS https://gitlab.com/tikiwiki/tikiwiki-ci-databases/raw/master/$DBFILE.gz -o doc/devtools/dbdiff/cache/$DBFILE.gz && gzip -d doc/devtools/dbdiff/cache/$DBFILE.gz'
    - php -d display_errors=On doc/devtools/check_schema_upgrade.php -m $DBVER -e $ENGINE --db1=$MYSQL_USER:$MYSQL_PASSWORD@mysql:tikiold --db2=$MYSQL_USER:$MYSQL_PASSWORD@mysql:tikinew
  after_script:
    - echo "SHOW CREATE DATABASE tikiold" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - echo "SHOW CREATE DATABASE tikinew" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - mysqldump --column-statistics=0  --default-character-set=utf8mb4 -u root --password=$MYSQL_ROOT_PASSWORD -h mysql tikiold > tikiold.sql
    - mysqldump --column-statistics=0  --default-character-set=utf8mb4 -u root --password=$MYSQL_ROOT_PASSWORD -h mysql tikinew > tikinew.sql
  dependencies:
    - composer
    - composer-dbdiff
  needs:
    - composer
    - composer-dbdiff
  artifacts:
    paths:
      - tikiold.sql
      - tikinew.sql
    expire_in: 7 days
    when: always

.template-tiki-schema-upgrade-mysql8: &template-tiki-schema-upgrade-mysql8
  <<: *template-tiki-schema-upgrade
  services:
    - name: ${DEPENDENCY_PROXY_PREFIX}$IMAGE
      command: [ "--default-authentication-plugin=mysql_native_password", "--explicit-defaults-for-timestamp=off" ]
      alias: mysql

.db-upgrade-22-mysql8:
  <<: *template-tiki-schema-upgrade-mysql8
  variables:
    DBFILE: ci_22.sql
    DBVER: 22
    IMAGE: ${MYSQL_8_IMAGE}
    ENGINE: InnoDB

db-upgrade-22:
  <<: *template-tiki-schema-upgrade
  variables:
    DBFILE: ci_22.sql
    DBVER: 22
    IMAGE: ${MYSQL_5_IMAGE}
    ENGINE: InnoDB

.db-upgrade-21-mysql8:
  <<: *template-tiki-schema-upgrade-mysql8
  variables:
    DBFILE: ci_21.sql
    DBVER: 21
    IMAGE: ${MYSQL_8_IMAGE}
    ENGINE: InnoDB

db-upgrade-21:
  <<: *template-tiki-schema-upgrade
  variables:
    DBFILE: ci_21.sql
    DBVER: 21
    IMAGE: ${MYSQL_5_IMAGE}
    ENGINE: InnoDB

.db-upgrade-20-mysql8:
  <<: *template-tiki-schema-upgrade-mysql8
  variables:
    DBFILE: ci_20.sql
    DBVER: 20
    IMAGE: ${MYSQL_8_IMAGE}
    ENGINE: InnoDB

db-upgrade-20:
  <<: *template-tiki-schema-upgrade
  variables:
    DBFILE: ci_20.sql
    DBVER: 20
    IMAGE: ${MYSQL_5_IMAGE}
    ENGINE: InnoDB

db-upgrade-19:
  <<: *template-tiki-schema-upgrade
  variables:
    DBFILE: ci_19.sql
    DBVER: 19
    IMAGE: ${MYSQL_5_IMAGE}
    ENGINE: InnoDB

db-upgrade-18-lts:
  <<: *template-tiki-schema-upgrade
  variables:
    DBFILE: ci_18.sql
    DBVER: 18
    IMAGE: ${MYSQL_5_IMAGE}
    ENGINE: InnoDB

db-upgrade-18-lts-myisam:
  <<: *template-tiki-schema-upgrade
  variables:
    DBFILE: ci_18.sql
    DBVER: 18
    IMAGE: mysql:5.5
    ENGINE: MyISAM

sql-engine-conversion:
  stage: tiki-check
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  services:
    - name: ${DEPENDENCY_PROXY_PREFIX}${MYSQL_5_IMAGE}
      alias: mysql
  script:
    - 'mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql --skip-column-names -B -e "SELECT CONCAT(''Version: '', VERSION(), CHAR(13), ''sql_mode: '', @@GLOBAL.sql_mode)"'
    - echo "GRANT ALL ON tikiold.* TO '${MYSQL_USER}';" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - echo "GRANT ALL ON tikinew.* TO '${MYSQL_USER}';" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - php -d display_errors=On doc/devtools/check_sql_engine_conversion.php --db1=$MYSQL_USER:$MYSQL_PASSWORD@mysql:tikiold --db2=$MYSQL_USER:$MYSQL_PASSWORD@mysql:tikinew
  after_script:
    - echo "SHOW CREATE DATABASE tikiold" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - echo "SHOW CREATE DATABASE tikinew" | mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql
    - mysqldump --column-statistics=0 -u root --password=$MYSQL_ROOT_PASSWORD -h mysql tikiold > tikiold.sql
    - mysqldump --column-statistics=0 -u root --password=$MYSQL_ROOT_PASSWORD -h mysql tikinew > tikinew.sql
  dependencies:
    - composer
    - composer-dbdiff
  needs:
    - composer
    - composer-dbdiff
  artifacts:
    paths:
      - tikiold.sql
      - tikinew.sql
    expire_in: 7 days
    when: always

#
# Unit Tests
#

.template-unit-tests: &template-unit-tests
  stage: unit-tests
  image: ${DEPENDENCY_PROXY_PREFIX}${IMAGE}
  services:
    - name: ${DEPENDENCY_PROXY_PREFIX}${MYSQL_5_IMAGE}
      alias: mysql
    - name: ${DEPENDENCY_PROXY_PREFIX}elasticsearch:5
      alias: elasticsearch
    - name: ${DEPENDENCY_PROXY_PREFIX}manticoresearch/manticore:dev
      alias: manticore
  variables:
    EXTRA: 1
    MCL: 1
  script:
    - 'echo -n "PHP Version: " && php -v | grep "^PHP"'
    - 'echo -n "MySQL/MariaDB Version: " && mysql -u root --password=$MYSQL_ROOT_PASSWORD -h mysql --skip-column-names -B -e "SELECT VERSION()" 2>/dev/null'
    - 'echo -n "ElasticSearch Version: " && curl -sS -XGET ''http://elasticsearch:9200'' | grep ''"number"'' | sed ''s/.*:.*"\(.*\)".*/\1/'''
    - 'echo -n "Manticore Version: " && mysql -h manticore -P 9306 -e "status" | grep "Server version:" | sed "s/^[^:]*:[[:space:]]*//"'
    - echo '<?php $db_tiki = "mysqli"; $dbversion_tiki = "21.0"; $host_tiki = "mysql"; $user_tiki = "tikiuser"; $pass_tiki = "tikipass"; $dbs_tiki = "tikitest"; $client_charset = "utf8mb4"; ' > lib/test/local.php
    - php -d display_errors=On vendor_bundled/vendor/phpunit/phpunit/phpunit --colors=always --log-junit report.xml
  dependencies:
    - composer
  needs:
    - composer
  artifacts:
    when: always
    paths:
      - report.xml
    reports:
      junit: report.xml
    expire_in: 7 days

.template-unit-tests-mysql8: &template-unit-tests-mysql8
  <<: *template-unit-tests
  services:
    - name: ${DEPENDENCY_PROXY_PREFIX}${MYSQL_8_IMAGE}
      command: [ "--default-authentication-plugin=mysql_native_password", "--explicit-defaults-for-timestamp=off" ]
      alias: mysql
    - name: ${DEPENDENCY_PROXY_PREFIX}elasticsearch:5
      alias: elasticsearch
    - name: ${DEPENDENCY_PROXY_PREFIX}manticoresearch/manticore:dev
      alias: manticore

unit-tests-81:
  <<: *template-unit-tests
  variables:
    IMAGE: xorti/ubuntu-php:8.1-qa
  allow_failure: false

unit-tests-81-mysql8:
  <<: *template-unit-tests-mysql8
  variables:
    IMAGE: xorti/ubuntu-php:8.1-qa
  allow_failure: false

#
# Package this Tiki build
#

tiki-package:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: package-tiki
  variables:
    COMPOSER_CACHE_DIR: ${CI_PROJECT_DIR}/.composercache
  script:
    # Remove dev dependencies in composer
    - echo "=> Removing Composer dev dependencies ..."
    - composer -V | grep "version 2" || composer self-update --2
    - composer --ansi install -d vendor_bundled --no-dev --optimize-autoloader --no-progress --prefer-dist -n # remove dev packages
    # Cleanup folders that may have data, and we do not want to be packed
    - echo "=> Cleanup ..."
    # Assure temp folder is empty
    - rm -fr temp && git checkout temp
    # remove bin directory
    - rm -fr bin
    # Log files that are not part of GIT in the folder (exclude the ones expected)
    - echo "=> Files in the folder not tracked in git ..."
    - git ls-files --others | grep -v vendor_bundled/vendor/ | grep -v .composercache/ || echo "Ignore Failure"
    # remove comments from language files
    - echo "=> Optimize language files ..."
    - find lang/ -name language.php -exec php doc/devtools/stripcomments.php {} \;
    # set Permissions
    - echo "=> Fix permissions ..."
    - find . -type f -exec chmod 0664 {} \;
    - chown 0775 setup.sh
    - find . -type d -exec chmod 0755 {} \;
    # pack
    - echo "=> Pack tiki ..."
    - tar --exclude *.DS_Store --exclude tests --exclude doc/devtools --exclude .git --exclude .gitignore --exclude .composercache -pczf tiki-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_SLUG}.tar.gz *
  cache:
    - key:
        prefix: "composer"
        files:
          - vendor_bundled/composer.lock
      paths:
        - .composercache/
      policy: pull
  dependencies:
    - composer
  needs:
    - composer
  artifacts:
    paths:
      - tiki-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_SLUG}.tar.gz
    expire_in: 30 days
  allow_failure: true

# Vendors Security
#

#disabled, showld be generated from /lib/core/Tiki/Package/ComposerPackages.yml
.vendor-security-check:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: vendors-security
  script:
    - composer -V | grep "version 2" || composer self-update --2
    - cp composer.json.dist composer.json
    # These are packages frequently installed with tiki, but not distributed with it
    # This should be generated from /lib/core/Tiki/Package/ComposerPackages.yml
    # It wasn't automated before, and I don't know how we were supposed to get some of these packages locally.  php console.php package:list can generate the list, but I can find no command to install them all - benoitg - 2023-05-04
    - composer require jerome-breton/casperjs-installer:dev-master --no-progress
    - composer require enygma/expose:^3.0 --no-progress
    - composer require fullcalendar/fullcalendar-scheduler ^1.9 --no-progress
    - composer require fakerphp/faker dev-master --no-progress
    - composer require google/apiclient ^2.2.2 --no-progress
    - composer require npm-asset/lozad ^1.6.0 --no-progress
    - composer require mpdf/mpdf ^8.0.0 --no-progress
    - composer require xorti/mxgraph-editor ^3.9.12.2 --no-progress
    - composer require tikiwiki/diagram ^10 --no-progress
    - composer require thiagoalessio/tesseract_ocr ^2.7.0 --no-progress
    - composer require mathjax/mathjax ^2.7 --no-progress
    - composer require media-alchemyst/media-alchemyst ^0.5.1 --no-progress
    - composer require npm-asset/pdfjs-dist ~2.0.487 --no-progress
    - composer require bower-asset/wodo.texteditor ^0.5.9 --no-progress
    - composer require j0k3r/php-readability ^1.1.10 --no-progress
    - cd vendor_bundled
    - php vendor/thibautselingue/local-php-security-checker-installer/bin/local-php-security-checker-installer
    - cd ..
    - bin/local-php-security-checker --path=composer.lock
  allow_failure: false
  dependencies:
    - composer
  needs:
    - composer
  only:
    refs:
      - schedules
    variables:
      - $SCHEDULER == "vendor_check"

vendor-bundled-security-check:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: vendors-security
  script:
    - composer -V | grep "version 2" || composer self-update --2
    - cd vendor_bundled
    - php vendor/thibautselingue/local-php-security-checker-installer/bin/local-php-security-checker-installer
    - cd ..
    - bin/local-php-security-checker --path=vendor_bundled/composer.lock
  dependencies:
    - composer
  needs:
    - composer
  #We do want our pipeline to spontaneously warn us if a security advisory is released, but we don't want it to stop the development process
  allow_failure: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - 'vendor_bundled/composer.json'
        - 'vendor_bundled/composer.lock'
    - if: $CI_COMMIT_BRANCH == "master"
      changes:
        - 'vendor_bundled/composer.json'
        - 'vendor_bundled/composer.lock'
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULER == "vendor_check"

update_vendor_bundled_dependencies:
  image: ${DEPENDENCY_PROXY_PREFIX}${BASE_QA_IMAGE}
  stage: vendors_update
  dependencies:
    - composer
  needs:
    - composer
  variables:
    BRANCH_NAME: "${CI_COMMIT_REF_NAME}_vendor_bundled_update"
    COMMIT_MESSAGE: "[UPD] Update ${CI_COMMIT_REF_NAME} vendor_bundled dependencies"
    CI_REPOSITORY_URL: "https://${GITLAB_USER_LOGIN}:${GITLAB_USER_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git"
    COMPOSER_DISCARD_CHANGES: "true"
    COMPOSER_NO_INTERACTION: "1"
  before_script:
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git config --global user.name "${GITLAB_USER_NAME}"
  script:
    - composer -V | grep "version 2" || composer self-update --2
    - DATE=`date +%Y%m%d`
    - BRANCH_NAME="${BRANCH_NAME}_${DATE}"
    - php doc/devtools/update_vendor_bundled.php
    - if git diff --quiet --exit-code vendor_bundled/composer.lock; then exit 0; fi;
    - if git rev-parse --verify ${BRANCH_NAME}; then git branch -D ${BRANCH_NAME}; fi;
    - git checkout -b ${BRANCH_NAME}
    - git add vendor_bundled/composer.lock
    - git commit -m "${COMMIT_MESSAGE}"
    - git push ${CI_REPOSITORY_URL} HEAD:${BRANCH_NAME} --quiet
    - |
      HOST=${CI_PROJECT_URL} CI_PROJECT_ID=${CI_PROJECT_ID} SOURCE_BRANCH=${BRANCH_NAME} TARGET_BRANCH=${CI_COMMIT_REF_NAME} GITLAB_USER_ID=${GITLAB_USER_ID} PRIVATE_TOKEN=${GITLAB_USER_TOKEN} TARGET_PROJECT_ID=${TARGET_PROJECT_ID} SET_MERGE=${SET_MERGE} \
      /bin/bash ./doc/devtools/gitlab/auto_merge_request.sh "${COMMIT_MESSAGE} (${DATE})"
  only:
    refs:
      - schedules
    variables:
      - $SCHEDULER == "update_vendor_bundled"
